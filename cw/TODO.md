# High priority

# Medium priority

* Find a way to let room-1 and room-11 have the same members `link 1 11`

# Low priority

* Add a command for the bot to import data to `guilds.json` and `details.json`. Maybe `import guilds`
* Add a way to record meta command (or macro, alias, etc)
* Implement `show [GUILD NAME], [GUILD ID], ...` to show guilds data
