# frozen_string_literal: true

require 'csv'
require 'time'
require 'discordrb'

class Porkbot
  Channels = [
    629575293316890624, # 大草原🌿
    629589541178572811, # 森林🌳
    629591564100042752, # 山脈⛰
    629647160782356481, # 湖泊🎣
    629659734986981386, # 海島🏝
    629650135198072852, # 沼澤💀
  ].freeze

  Gateway = 629683866042368020 # 傳送門
  Boss = {
    548843096394170369 => '@&623863788244041776 ', # 煞氣ㄟ台灣—王鬧鐘
    560400863995363328 => '@&561232627454050325 '  # member
  }.freeze

  attr_reader :bot, :mutex, :pokemon_map, :pokemon_names, :async_timers

  def initialize
    @bot = Discordrb::Bot.new(token: ENV['DISCORD_TOKEN'])
    @mutex = Mutex.new
    @pokemon_map = generate_pokemon_map
    @pokemon_names = pokemon_map.keys
    @async_timers = {}
  end

  def hook
    hook_pokemon
    hook_guess
    hook_remind
    hook_boss
    hook_timers
    hook_cancel
    hook_ciel
    hook_charlotte
    hook_echo
  end

  def run
    bot.run
  end

  private

  def hook_pokemon
    last_channel = picked = nil

    bot.message(with_text: '‌‌A wild pokémon has аppeаred!') do |event|
      picked = Channels.sample until last_channel != picked

      retry_request do
        bot.send_message(Gateway, "p!redirect <##{picked}>")
      end

      last_channel = picked
    end
  end

  def hook_guess
    syntax = /\Ap\?guess\s+(.+)\s*\z/i

    bot.message(with_text: syntax) do |event|
      hint = event.message.content[syntax, 1].tr('_', '.')
      search =
        if hint.include?('.')
          /\A#{hint}\z/i
        else
          /#{hint}/i
        end

      message = grep_pokemon(search).join("\n")

      send_message(event, message)
    end
  end

  def hook_remind
    syntax = /\Ap\?remind\s+(.+?)\s+(.+)\z/i

    bot.message(with_text: syntax) do |event|
      process_remind(event, syntax)
    end
  end

  def hook_boss
    syntax = /\Ap\?boss\s+(.+?)(?:\s+(.+))?\z/i

    bot.message(with_text: syntax) do |event|
      _, message = Boss.find do |server_id, _|
        server_id == event.server.id
      end

      if message
        process_remind(event, syntax, message)
      else
        send_message(event, 'Cannot recognize this Discord server')
      end
    end
  end

  def hook_timers
    syntax = /\Ap\?timers\s*\z/i

    bot.message(with_text: syntax) do |event|
      now = Time.now

      # Reduce locking time by iterating the read-only duplicated timer
      message = with_timers(&:dup).map do |timer_id, thread|
        elaped = now - thread[:time_when_sleep]
        remaining_time = duration(thread[:time_to_sleep] - elaped, now)
        timer_name = timer_id.to_s(36)

        "`#{timer_name}` in #{remaining_time} with #{thread[:message]}" \
          " in #{thread[:event].channel.mention}"
      end.join("\n")

      send_message(event, message)
    end
  end

  def hook_cancel
    syntax = /\Ap\?cancel\s+(.+)\s*\z/i

    bot.message(with_text: syntax) do |event|
      timer_id = event.message.content[syntax, 1].to_i(36)

      with_timers do |timers|
        timers.delete(timer_id)&.wakeup
      end
    end
  end

  def hook_ciel
    last_ciel = Time.now

    syntax = /.*(?:希爾|\bciel\b).*/i

    bot.message(with_text: syntax) do |event|
      if last_ciel < Time.now - 600
        last_ciel = Time.now

        send_message(event, '<:ciel:638003424692011028>')
      end
    end
  end

  def hook_charlotte
    last_charlotte = Time.now

    syntax = /.*(?:龍砲|龍炮|龍泡|ㄌㄆ|\bcharlotte\b).*/i

    bot.message(with_text: syntax) do |event|
      if last_charlotte < Time.now - 600
        last_charlotte = Time.now

        send_message(event, '<:charlotte:714420716296077353>')
      end
    end
  end

  def hook_echo
    syntax = /\Ap\?echo\s+(.+)\s*\z/i

    bot.message(with_text: syntax) do |event|
      message = event.message.content[syntax, 1].
        gsub(/(?:\w+\:\d+)/, '<:\\0>')

      puts message

      send_message(event, message)
    end
  end

  def grep_pokemon search
    pokemon_names.grep(search).map do |candidate|
      case candidate
      when /\p{han}/
        "↓ #{candidate}\np!catch #{pokemon_map[candidate]}"
      else
        "↓ #{pokemon_map[candidate]}\np!catch #{candidate}"
      end
    end
  end

  def process_remind event, syntax, prefix=''
    syntax_timer = /(?:(\d+)d)?\s*(?:(\d+)h)?\s*(?:(\d+)m)?\s*(?:(\d+)s)?/i

    delay, message = event.message.content.match(syntax)[1..2]
    d, h, m, s = delay.match(syntax_timer)[1..4].map(&:to_i)
    time_to_sleep = d * 86400 + h * 3600 + m * 60 + s

    if time_to_sleep > 0
      handle_timer(event, time_to_sleep, "#{prefix}#{message}".strip)
    else
      send_message(
        event, "Bad format. Example: `p?remind 1h10m25s Hello, world!`")
    end
  end

  def handle_timer event, time_to_sleep, message
    timer_id = register_timer(event, time_to_sleep, message) do |alt_message|
      retry_request do
        actual_message =
          alt_message || message.gsub(/(?:@\&?|#)\d+/, '<\\0>')

        send_message(event, actual_message)
      end
    end

    send_message(
      event,
      "Reminding in #{duration(time_to_sleep)}." \
      " Cancel with: `p?cancel #{timer_id.to_s(36)}`")
  end

  def register_timer event, time_to_sleep, message, &callback
    with_timers do |timers|
      timer_thread = generate_timer(time_to_sleep, &callback)
      timer_id = timer_thread.object_id

      timer_thread[:event] = event
      timer_thread[:time_when_sleep] = Time.now
      timer_thread[:time_to_sleep] = time_to_sleep
      timer_thread[:message] = message

      timers[timer_id] = timer_thread

      timer_id
    end
  end

  def generate_timer time_to_sleep
    Thread.new do
      sleep(time_to_sleep)

      # Make sure :time_when_sleep was set by avoiding this running before
      # it was set.
      elaped = with_timers { Time.now - Thread.current[:time_when_sleep] }
      timer_id = Thread.current.object_id

      if elaped >= time_to_sleep
        yield(nil)
      else
        remaining_time = duration(time_to_sleep - elaped)

        yield("Removed `#{timer_id.to_s(36)}` in #{remaining_time}")
      end

      with_timers do |timers|
        timers.delete(timer_id)
      end
    end
  end

  def with_timers
    mutex.synchronize do
      yield(async_timers)
    end
  end

  def send_message event, message
    if message.empty?
      retry_request do
        event.send_message("Nothing found!")
      end
    else
      retry_request do
        event.send_message(message[0, 2000])
      end
    end
  end

  def retry_request retries=3
    yield
  rescue RestClient::Exception => e
    puts "Error: #{e.class}:#{e.message}, retries: #{retries}"

    if retries > 0
      sleep(0.1)
      retry_request(retries - 1)
    end
  end

  def duration delta, now=Time.now
    result = []

    [[ 60, 'seconds'],
     [ 60, 'minutes'],
     [ 24, 'hours'  ],
     [365, 'days'   ],
     [999, 'years'  ]].
      inject(delta.round) do |length, (divisor, name)|
        quotient, remainder = length.divmod(divisor)

        if remainder > 0
          case remainder
          when 1
            result.unshift("#{remainder} #{name.chomp('s')}")
          else
            result.unshift("#{remainder} #{name}")
          end
        end

        break if quotient == 0

        quotient
      end

    time = now + delta
    timestamp = Time.at(time, in: '+08:00').strftime('%H:%M%:z')
    relative_time = result.join(' ')
    utc = time.utc.strftime('%H:%MZ')

    "#{timestamp} (#{relative_time}) (#{utc})"
  end

  def generate_pokemon_map
    csv = File.read("#{__dir__}/pokemon.csv")
    id, en, zh = CSV.parse(csv).drop(1).transpose
    en2zh = Hash[en.zip(zh)]

    en2zh.merge(en2zh.invert)
  end
end

bot = Porkbot.new
bot.hook
bot.run
